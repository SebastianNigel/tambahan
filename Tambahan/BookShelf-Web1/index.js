function addData() {
    let name = document.getElementById("name").value;
    let author = document.getElementById("author").value;
    let publisher = document.getElementById("publisher").value;
    let yearofissue = document.getElementById("yearofissue").value;
    let description = document.getElementById("description").value;
    let price = document.getElementById("price").value;
    
    let jsonStr = JSON.stringify({
        name: name,
        author: author,
        publisher: publisher,
        yearofissue: yearofissue,
        description: description,
        price: price
    });

    fetch('http://localhost:1414/add', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: jsonStr,
    })
        .then(response => response.json())
        .then(data => {
            console.log('Success:', data);
            window.location.href = "database.html"
        })
        .catch((error) => {
            console.error('Error:', error);
        });
    getData("http://localhost:1414/books")
}

function deleteData(id) {
    let del = confirm("Sure to delete this data ?")
    if (del) {
        fetch(`http://localhost:1414/delete/${id}`, {
            method: 'DELETE',
        })
            .then(() => location.reload())
            .catch(console.error)
    }
}

function getData(url) {
    let http = new XMLHttpRequest();
    http.open("GET", url, true)
    http.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            let res = JSON.parse(this.responseText)
            console.log(res.length)

            let msgReceipt = ""
            let no = 1;
            for (let i = 0; i < res.length; i++) {
                msgReceipt += `
<tr>
                    <td>${no}</td>
                    <td>${res[i].name}</td>
                    <td>${res[i].author}</td>
                    <td>${res[i].publisher}</td>
                    <td>${res[i].yearofissue}</td>
                    <td>
                    
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#${res[i].id}det">
                    DETAIL
                  </button>

                <div class="modal fade" id="${res[i].id}det" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">DETAIL DATA EMPLOYEE</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <form>
                                        <label style="font-weight: bold" for="name">Book Name</label>
                                        <br>
                                        ${res[i].name}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="author">Author</label>
                                        <br>
                                        ${res[i].author}
                                    </div>                                 
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="publisher">Publisher</label>
                                        <br>
                                       ${res[i].publisher}
                                    </div>

                                    <div class="form-group">
                                        <label style="font-weight: bold" for="yearofissue">Released Year</label>
                                        <br>
                                        ${res[i].yearofissue}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="description">Description</label>
                                        <br>
                                        ${res[i].description}
                                    </div>
                                    <div class="form-group">
                                        <label style="font-weight: bold" for="price">Price</label>
                                        <br>
                                        ${res[i].price}
                                    </div>
                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>



                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#${res[i].id}up">
                UPDATE
              </button>

                <div class="modal fade" id="${res[i].id}up" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Update Data</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
<input type="hidden" value="${res[i].id}" id="id">
                                    
                    <form style="font-weight: bold">  
                                    <div class="form-group">
                                        <label for="name">Book Name</label>
                                        <input type="text" class="form-control" id="edit-name" value="${res[i].name}" />
                                    </div>
                                    <div class="form-group">
                                        <label for="author">Author</label>
                                        <input type="text" class="form-control" id="edit-author" value="${res[i].author}" />
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="publisher">Publisher</label>
                                        <input type="text" class="form-control" id="edit-publisher" value="${res[i].publisher}" />
                                    </div>

                                    <div class="form-group">
                                        <label for="yearofissue">Released Year</label>
                                        <input type="text" class="form-control" id="edit-yearofissue" value="${res[i].yearofissue}" />
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="description">Description</label>
                                        <input type="text" class="form-control" id="edit-description" value="${res[i].description}" />
                                    </div>

                                    <div class="form-group">
                                        <label for="price">Price</label>
                                        <input type="text" class="form-control" id="edit-price" value="${res[i].price}" />
                                    </div>

                                    <div class="modal-footer">
                                        <button type="reset" class="btn btn-secondary" data-dismiss="modal">
                                            Close
                                        </button>
                            
                                        <button type="submit" class="btn btn-primary" onclick="updateData(${res[i].id})">Update Data</button>
                                    </div>

                            </div>
                    </form>
                        </div>
                    </div>
                </div>


                    <button type="button" class="btn btn-danger mb-1" onclick="deleteData(${res[i].id})">DELETE</button>
</td>
</tr>
                `
                no++;
            }
            document.getElementById("booksBody").innerHTML = msgReceipt
        }
    }
    http.send()
}

function updateData() {
    let name = document.getElementById("edit-name").value;
    let author = document.getElementById("edit-author").value;
    let publisher = document.getElementById("edit-publisher").value;
    let yearofissue = document.getElementById("edit-yearofissue").value;
    let description = document.getElementById("edit-description").value;
    let price = document.getElementById("edit-price").value;
   
    let jsonStr = JSON.stringify({
        name: name,
        author: author,
        publisher: publisher,
        yearofissue: yearofissue,
        description: description,
        price: price
    });


    fetch('http://localhost:1414/update', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: jsonStr,
    })
        .then(response => response.json())
        .then(() => {
            console.log(jsonStr)
        })
        .catch((error) => {
            console.error('Error:', error);
        });
}

getData("http://localhost:1414/books")