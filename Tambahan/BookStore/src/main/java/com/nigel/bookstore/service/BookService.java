package com.nigel.bookstore.service;

import com.nigel.bookstore.entity.Book;
import com.nigel.bookstore.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookService {
    @Autowired
    private BookRepository repository;

    public Book saveBook(Book book){return repository.save(book);}
    public List<Book> getBooks(){return repository.findAll();}
    public Book getBookById(int id) {return repository.findById(id).orElse(null);}
    public Book getBookByName(String name){return repository.findByName(name);}

    public String deleteBook(int id){
    repository.deleteById(id);
    return "This Book Has Been Deleted!";
    }

    public Book updateBook(Book book){
    Book existingBook = repository.findById(book.getId()).orElse(null);
    existingBook.setId(book.getId());
    existingBook.setName(book.getName());
    existingBook.setAuthor(book.getAuthor());
    existingBook.setPublisher(book.getPublisher());
    existingBook.setYearofissue(book.getYearofissue());
    existingBook.setDescription(book.getDescription());
    existingBook.setPrice(book.getPrice());
    return repository.save(existingBook);
    }
}
