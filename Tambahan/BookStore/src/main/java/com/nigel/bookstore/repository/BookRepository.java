package com.nigel.bookstore.repository;


import com.nigel.bookstore.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Integer> {
    Book findByName(String name);
}
