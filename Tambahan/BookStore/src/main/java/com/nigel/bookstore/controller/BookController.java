package com.nigel.bookstore.controller;

import com.nigel.bookstore.entity.Book;
import com.nigel.bookstore.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*",allowedHeaders = "*")
@RestController
public class BookController {
    @Autowired
    private BookService service;

    @PostMapping("/add")
    public Book addBook(@RequestBody Book book){return service.saveBook(book);}

    @GetMapping("/books")
    public List<Book> findAllBooks(){return service.getBooks();}

    @GetMapping("/bookById/{id}")
    public Book findBearById(@PathVariable int id) {
        return service.getBookById(id);
    }

    @GetMapping("/bookByName/{name}")
    public Book findBearByName(@PathVariable String name) {return service.getBookByName(name);}

    @PutMapping("/update")
    public Book updateBook(@RequestBody Book book){return service.updateBook(book);}

    @DeleteMapping("/delete/{id}")
    public String deleteBookById(@PathVariable int id){return service.deleteBook(id);}

}
